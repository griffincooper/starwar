﻿using UnityEngine;
using System.Collections;

public class Player2Right : MonoBehaviour {

	public float speed = 5f;
	Vector3 targetPos;
	Vector3 playerpos;
	GameObject ballObj;


	void Update () 
	{
		ballObj = GameObject.FindGameObjectWithTag ("Ball");
		if (ballObj != null) 
		{
			targetPos = Vector3.Lerp (gameObject.transform.position, ballObj.transform.position, Time.deltaTime * speed);
			//Debug.Log ("Player2Right:Update:targetPos=" + targetPos.ToString ());
            playerpos = new Vector3(-44.4f, Mathf.Clamp(targetPos.y, -40.1f, -1.16f), 0);
            gameObject.transform.position = new Vector3(44.4f, playerpos.y, 0);
			
		}

	}
}
