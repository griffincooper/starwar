﻿using UnityEngine;
using System.Collections;

public class SaberController : MonoBehaviour {

	public Score.PLAYER currentPlayer;
	public float LightSaberSpeed = 1f;
	public Vector3 LightSaberpos=new Vector3(0,0,0);
	float yPos = 0f;

    private float normalSpeed = 1f;
    private float slowSpeed = 0.25f;
    public bool isSpeedSlow = false;
    private float slowSpeedTimeLimit = 5f;
    private float slowSpeedTimer = 0f;

    public bool testSlow = false;

    void Update () 
	{
		if (currentPlayer == Score.PLAYER.PLAYER1) {
			yPos = gameObject.transform.position.y + (Input.GetAxis ("Vertical") * LightSaberSpeed * (isSpeedSlow ? slowSpeed : normalSpeed));
		} else if (currentPlayer == Score.PLAYER.PLAYER2) {
			if (Input.GetKey (KeyCode.I)) {
				yPos = gameObject.transform.position.y + (1f * LightSaberSpeed * (isSpeedSlow ? slowSpeed : normalSpeed));
			}
			else if (Input.GetKey (KeyCode.K)) {
				yPos = gameObject.transform.position.y + (-1f * LightSaberSpeed * (isSpeedSlow ? slowSpeed : normalSpeed));
			}
		}
		LightSaberpos = new Vector3 (currentPlayer == Score.PLAYER.PLAYER1 ? -21.7f : 45.5f, 
									 Mathf.Clamp (yPos, -40.8f, -0.53f), 
									 0);
		gameObject.transform.position = LightSaberpos;

        if (isSpeedSlow)
        {
            slowSpeedTimer += Time.deltaTime;
            if (slowSpeedTimer >= slowSpeedTimeLimit)
            {
                isSpeedSlow = false;
            }
        }

        if (testSlow)
        {
            ReduceSpeed();
            testSlow = false;
        }
    }

    public void ReduceSpeed()
    {
        isSpeedSlow = true;
        slowSpeedTimer = 0f;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "bullet")
        {
            Destroy(other.gameObject);
            ReduceSpeed();
        }
    }
}
