﻿using UnityEngine;
using System.Collections;

public class Ball2 : MonoBehaviour {

	public float ballVel = 300;
	Rigidbody rb;
	bool isPlay;

	int randInt;

	void Awake()
	{
		rb = gameObject.GetComponent<Rigidbody> ();
		randInt = Random.Range (1, 3);

	}

	void Update () 
	{
		if(Input.GetKey(KeyCode.R) && isPlay == false)
		{
			transform.parent = null;
			isPlay = true;
			rb.isKinematic = false;
			if( randInt== 1)
			{
				rb.AddForce (new Vector3 (ballVel, ballVel, 0));
			}
			if (randInt == 2) 
			{
				rb.AddForce (new Vector3 (-ballVel, -ballVel, 0));
			}

		}

	}
}
