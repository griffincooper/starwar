﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BallController : MonoBehaviour {

	public GameObject ballPrefab;
    private List<Ball> gameBalls = new List<Ball>();
    public int numBallsToSpawn = 2;

    public Transform P1paddleobj;
    public Transform P2paddleobj;

    public void RemoveBall(Ball b)
    {
        gameBalls.Remove(b);
    }

    public bool AreAnyBallsInPlay()
    {
        bool anyInPlay = false;
        foreach (Ball b in gameBalls)
        {
            if (b != null)
            {
                anyInPlay |= b.GetIsInPlay();
            }
        }
        return anyInPlay;
    }

    public void SpawnBalls(bool isPlayerOne)
    {
        SpawnBalls(numBallsToSpawn, isPlayerOne);
    }

    public void SpawnBalls(int numberOfBalls, bool isPlayerOne)
    {
        for (int i = 0; i < numberOfBalls; i++)
        {
            GameObject ballObj = null;
            if (isPlayerOne)
            {
                ballObj = Instantiate(ballPrefab, new Vector3(P1paddleobj.transform.position.x + 2, P1paddleobj.transform.position.y + (float)(numberOfBalls-1-i)*10f, 0), Quaternion.identity) as GameObject;
                ballObj.name = "Ball-" + (i+1) + "/" + numberOfBalls + "_1";
                ballObj.transform.parent = P1paddleobj;
            }
            else
            {
                ballObj = Instantiate(ballPrefab, new Vector3(P2paddleobj.transform.position.x - 2, P2paddleobj.transform.position.y + (float)(numberOfBalls - 1 - i) * 10f, 0), Quaternion.identity) as GameObject;
                ballObj.name = "Ball-" + (i + 1) + "/" + numberOfBalls + "_2";
                ballObj.transform.parent = P2paddleobj;
            }
            gameBalls.Add(ballObj.GetComponent<Ball>());
            ballObj.GetComponent<Ball>().SetForce(i,numBallsToSpawn);
            //newBall.GetComponent<Ball>().AddForce();
        }
    }

    /*
	public GameObject origBall;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.D)) {
			SpawnBalls (origBall.transform, 2);
		}
	}

	public void SpawnBalls(Transform trans, int numberOfBalls)
	{
		for (int i = 0; i < numberOfBalls; i++) {
			GameObject newBall = (GameObject) Instantiate (ballPrefab, trans.position, Quaternion.identity);
			newBall.transform.localScale = origBall.transform.localScale;
			newBall.transform.parent = origBall.transform.parent;

			newBall.GetComponent<Ball> ().AddForce ();
		}
	}
    */
}
