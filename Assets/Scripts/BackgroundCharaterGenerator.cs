﻿using UnityEngine;
using System.Collections;

public class BackgroundCharaterGenerator : MonoBehaviour {

	//the nums of object
	public static int NUM_OBJECTS = 2;
	public Transform node1;
	public GameObject block;
	private GameObject[] blocks = new GameObject[NUM_OBJECTS];

	public Transform topMarker;
	public Transform bottomMarker;
	public Transform terminateMarker;
	public Transform parentTransform;

	// Use this for initialization
	void Start () 
	{
		StartCoroutine( spawnBlock () );
	
	}
	
	// Update is called once per frame
	void Update () {
		foreach (GameObject go in blocks) 
		{
			if (go != null) {
				go.transform.localPosition += new Vector3 (1f, 0f, 0f);
				if (go.transform.position.x > terminateMarker.position.x) {
					go.transform.position = new Vector3 (topMarker.position.x, Random.Range (bottomMarker.position.y, topMarker.position.y), go.transform.position.z);
				}
			}
		}
	}

	IEnumerator spawnBlock()
	{
		for(int x = 0; x<NUM_OBJECTS;x++)
		{
			yield return new WaitForSeconds(10);

			GameObject go = (GameObject) Instantiate(block,node1.transform.position,node1.transform.rotation);
			go.transform.position = new Vector3 (topMarker.position.x, Random.Range (bottomMarker.position.y, topMarker.position.y), go.transform.position.z);
			go.transform.parent = parentTransform;
			blocks [x] = go;
		}

	}
}
