﻿using UnityEngine;
using System.Collections;

public class Bullets : MonoBehaviour 
{

	public GameObject Bullet_Emitter;

	public GameObject Bullet;

	public float Bullet_Forward_Force;

    public bool isLeftPlayer = true;



	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{
		if ((isLeftPlayer && Input.GetKeyDown("space")) ||
            (!isLeftPlayer && Input.GetKeyDown(KeyCode.RightControl)))
		{
            GameObject bullet = Instantiate(Bullet, transform.position + (isLeftPlayer ? 1f : -1f)*(new Vector3(4f,0f,0f)), Quaternion.identity) as GameObject;
            //bullet.transform.SetParent(transform); // Uncommenting this makes bullets travel with the paddle
            bullet.GetComponent<Rigidbody>().AddForce((isLeftPlayer ? Vector3.right : Vector3.left) * Bullet_Forward_Force);
            Destroy(bullet, 10f);
          
		}

	}
}