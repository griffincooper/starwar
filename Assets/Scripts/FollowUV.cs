﻿using UnityEngine;
using System.Collections;

public class FollowUV : MonoBehaviour {

	MeshRenderer mr;

	Material mat;

	Vector2 offset;
	public float parralax = 2f;

	public Transform transformToFollow;

	// Update is called once per frame
	void Awake()
	{
		mr = GetComponent<MeshRenderer>();

		mat = mr.material;

		offset = mat.mainTextureOffset;
	}
	void Update () 
	{
		offset.x = transformToFollow.position.x / transformToFollow.localScale.x / parralax;
		offset.y = transformToFollow.position.y / transformToFollow.localScale.y / parralax;

		mat.mainTextureOffset = offset;

	}
}
