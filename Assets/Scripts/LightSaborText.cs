﻿using UnityEngine;
using System.Collections;

public class LightSaborText : MonoBehaviour {
	LineRenderer lineRend;
	public Transform startpos;
	public Transform endpos;

	// Use this for initialization
	void Start () 
	{
		lineRend = GetComponent<LineRenderer> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		lineRend.SetPosition (0, startpos.position);
		lineRend.SetPosition (1, endpos.position);
	
	}
}
