﻿using UnityEngine;
using System.Collections;

public class Player1Left : MonoBehaviour {


	public float LightSaborSpeed = 1f;
	public Vector3 LightSaborpos=new Vector3(0,0,0);

    private float normalSpeed = 1f;
    private float slowSpeed = 0.25f;
    public bool isSpeedSlow = false;
    private float slowSpeedTimeLimit = 5f;
    private float slowSpeedTimer = 0f;

    public bool testSlow = false;
	void Update () 
	{
		float yPos = gameObject.transform.position.y + (Input.GetAxis ("Vertical") * LightSaborSpeed * (isSpeedSlow ? slowSpeed : normalSpeed));
		LightSaborpos = new Vector3 (-21.7f, Mathf.Clamp (yPos, -40.8f, -0.53f), 0);
		gameObject.transform.position = LightSaborpos;

        if (isSpeedSlow)
        {
            slowSpeedTimer += Time.deltaTime;
            if (slowSpeedTimer >= slowSpeedTimeLimit)
            {
                isSpeedSlow = false;
            }
        }

        if (testSlow)
        {
            ReduceSpeed();
            testSlow = false;
        }
	}

    public void ReduceSpeed()
    {
        isSpeedSlow = true;
        slowSpeedTimer = 0f;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "bullet")
        {
            Destroy(other.gameObject);
            ReduceSpeed();
        }
    }
}
