﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour {

	public enum PLAYER {
		PLAYER1,
		PLAYER2
	};
    public PLAYER player;

	public TextMesh currentP1Score;
	public TextMesh currentP2Score;
	public GameObject ballPerf;
    public BallController ballController;
	public Transform P1paddleobj;
	public Transform P2paddleobj;

	public int P1score;
	public int P2score;

    int winningScore = 10;

    public CharacterPopup lukeSkywalker;
    public CharacterPopup chewbacca;
    public CharacterPopup yoda;
    public CharacterPopup hanSolo;
    public CharacterPopup darthVader;
    public CharacterPopup palpatine;
    public CharacterPopup stormTrooper;
    public CharacterPopup darthMaul;

    public GameObject gameOverObject;

    public enum GameState
    {
        PLAYING,
        GAMEOVER
    }
    public GameState currentState = GameState.PLAYING;

	void Update ()
	{
        if (player == PLAYER.PLAYER1)
        {
            currentP1Score.text = "" + P1score;
        }
        else
        {
            currentP2Score.text = "" + P2score;
        }
	
	}

    private void Start()
    {
        if (ballController != null && player == PLAYER.PLAYER1)
        {
            if (!ballController.AreAnyBallsInPlay())
            {
                Debug.Log("Spawn Multiball from Start");
                ballController.SpawnBalls(true);
            }
        }
    }

	public void UpdateScore(PLAYER p, GameObject ball)
	{
		if (p == PLAYER.PLAYER1) {
			P1score += 1;
            if (P1score > P2score)
            {
                yoda.Popup();
                stormTrooper.Popup();
            }
            else if (P1score < P2score)
            {
                lukeSkywalker.Popup();
                darthVader.Popup();
            }
            if (ballController != null)
            {
                ballController.RemoveBall(ball.GetComponent<Ball>());
            }
            Destroy (ball);
            if (ballController != null)
            {
                Debug.Log("1Any balls in play? " + ballController.AreAnyBallsInPlay());
                if (!ballController.AreAnyBallsInPlay())
                {
                    Debug.Log("Spawn Multiball from UpdateScore (P1)");
                    ballController.SpawnBalls(true);
                }
            }
            else
            {
                (Instantiate(ballPerf, new Vector3(P1paddleobj.transform.position.x + 2, P1paddleobj.transform.position.y, 0), Quaternion.identity) as GameObject).transform.parent = P1paddleobj;
            }
		}
		else if (p == PLAYER.PLAYER2)
		{
			P2score += 1;
            if (P1score > P2score)
            {
                yoda.Popup();
                stormTrooper.Popup();
            }
            else if (P1score < P2score)
            {
                lukeSkywalker.Popup();
                darthVader.Popup();
            }
            if (ballController != null)
            {
                ballController.RemoveBall(ball.GetComponent<Ball>());
            }
            Destroy (ball);
            if (ballController != null)
            {
                Debug.Log("2Any balls in play? " + ballController.AreAnyBallsInPlay());
                if (!ballController.AreAnyBallsInPlay())
                {
                    Debug.Log("Spawn Multiball from UpdateScore (P2)");
                    ballController.SpawnBalls(false);
                }
            }
            else
            {
                (Instantiate(ballPerf, new Vector3(P2paddleobj.transform.position.x - 2, P2paddleobj.transform.position.y, 0), Quaternion.identity) as GameObject).transform.parent = P2paddleobj;
            }
		}

        if (P1score >= winningScore)
        {
            hanSolo.Popup();
            chewbacca.Popup();
            // Add Confetti here
            gameOverObject.SetActive(true);
            currentState = GameState.GAMEOVER;
            //Time.timeScale = 0f; // Pause
            LoadNextScene();
        }
        else if (P2score >= winningScore)
        {
            darthMaul.Popup();
            palpatine.Popup();
            gameOverObject.SetActive(true);
            currentState = GameState.GAMEOVER;
            //Time.timeScale = 0f; // Pause
            LoadNextScene();
        }
	}

    public void LoadNextScene()
    {
        int sceneNumber = SceneManager.GetActiveScene().buildIndex;
        switch (sceneNumber)
        {
            case 0:
            case 4:
            default:
                SceneManager.LoadScene(1);
                break;
            case 1:
                SceneManager.LoadScene(2);
                break;
            case 2:
                SceneManager.LoadScene(3);
                break;
		case 3:
			SceneManager.LoadScene(4);
			break;
        }
    }
}
