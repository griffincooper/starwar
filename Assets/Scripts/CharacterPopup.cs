﻿using UnityEngine;
using System.Collections;

public class CharacterPopup : MonoBehaviour {

    public Animator animator;

    public bool test = false;
	// Update is called once per frame
	void Update () {
	    if (test)
        {
            Popup();
            test = false;
        }
	}

    public void Popup()
    {
        animator.enabled = true;
        animator.SetTime(0);
        animator.Play("PopupAnimation");
    }
    
}
