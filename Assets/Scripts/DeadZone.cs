﻿using UnityEngine;
using System.Collections;

public class DeadZone : MonoBehaviour {

	public Score.PLAYER thisPlayer;
	public Score score;

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Ball") 
		{
			score.UpdateScore (thisPlayer, other.gameObject);
            if (score.currentState == Score.GameState.PLAYING)
            {
                other.GetComponent<Ball>().SetOutOfPlay();
            }
		}
	}
}
