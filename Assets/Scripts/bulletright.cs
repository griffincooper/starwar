﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletright : MonoBehaviour {

	public GameObject Bullet_Emitter;

	public GameObject Bullet;

	public float Bullet_Forward_Force;

	public bool isRightPlayer = true;



	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{
		if ((isRightPlayer && Input.GetKeyDown("l")) ||
			(!isRightPlayer && Input.GetKeyDown(KeyCode.RightControl)))
		{
			GameObject bullet = Instantiate(Bullet, transform.position + (isRightPlayer ? 1f : -1f)*(new Vector3(-4f,0f,0f)), Quaternion.identity) as GameObject;
			//bullet.transform.SetParent(transform); // Uncommenting this makes bullets travel with the paddle
			bullet.GetComponent<Rigidbody>().AddForce((isRightPlayer ? Vector3.left : Vector3.left) * Bullet_Forward_Force);
			Destroy(bullet, 10f);

		}

	}
}
