﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {


	public float ballVel = 300;
	Rigidbody rb;
	bool isPlay;

    Vector3 force = Vector3.zero;

	void Awake()
	{
		rb = gameObject.GetComponent<Rigidbody> ();        
	}

    public void SetForce(float absXVel, float absYVel)
    {
        SaberController sc = GetComponentInParent<SaberController>();
        if (sc.currentPlayer == Score.PLAYER.PLAYER1)
        {
            force = (new Vector3(absXVel, absYVel, 0));
        }
        else
        {
            force = (new Vector3(-absXVel, -absYVel, 0));
        }
    }

    public void SetForce(int index, int max)
    {
        if (max == 2)
        {
            if (index == 0)
            {
                SetForce(ballVel, ballVel);
            }
            else
            {
                SetForce(ballVel, -ballVel);
            }
        }
    }
		
	void Update () 
	{
		if(Input.GetKey(KeyCode.R) && isPlay == false)
		{
			AddForce ();	
		}
	
	}

    public void SetOutOfPlay()
    {
        isPlay = false;
    }

    public bool GetIsInPlay()
    {
        return isPlay;
    }

    public void AddForce()
    {
		isPlay = true;
		rb.isKinematic = false;
        if (force == Vector3.zero)
        {
            SetForce(ballVel, ballVel);
        }
        rb.AddForce(force);
        transform.parent = null;
    }

}
