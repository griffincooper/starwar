﻿using UnityEngine;
using System.Collections;

public class SpaceBackground2 : MonoBehaviour {


	public float speed = 10f;
	public float parralx = 2f;


	void Update () 
	{
		MeshRenderer mr = GetComponent<MeshRenderer> ();

		Material mat = mr.material;

		Vector2 offset = mat.mainTextureOffset;
		 
		offset.x += transform.position.x / transform.localScale.x;
		offset.x += transform.position.y / transform.localScale.y;

		mat.mainTextureOffset = offset;


	}
}
