﻿using UnityEngine;
using System.Collections;

public class SpaceBackGround : MonoBehaviour {

	public float speed;
	// Update is called once per frame
	void Update () 
	{
		MeshRenderer mr = GetComponent<MeshRenderer> ();

		Material mat = mr.material;

		Vector2 offset = mat.mainTextureOffset;

		offset.x += Time.deltaTime / speed;

		mat.mainTextureOffset = offset;
	
	
	}
}
